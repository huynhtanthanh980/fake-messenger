

import 'package:flutter/material.dart';

class CustomColor {

  //messengerScreen
  static Color messengerScreenBackgroundColor = Color(0xff010101);
  static Color messengerScreenActionButtonColor = Color(0xff333333);

  static Color messengerScreenSearchText = Color(0xffA1A1A1);
  static Color messengerScreenSearchBox = messengerScreenActionButtonColor;

  static Color messengerScreenNoAvaButton = messengerScreenActionButtonColor;

  static Color messengerScreenOnlineIndicator = Color(0xff53C833);

  static Color messengerScreenOnlineBubbleBackground = Color(0xffF83F3E);
  static Color messengerScreenOnlineNumberText = Color(0xffFFFFFF);
  static Color messengerScreenPeopleBubbleBackground = Color(0xff25491C);
  static Color messengerScreenPeopleTextNumber = Color(0xff8EE462);

  static Color messengerScreenNavigatorButtonOn = Color(0xffFFFFFF);
  static Color messengerScreenNavigatorButtonOff = Color(0xff656C74);

  static Color messengerScreenLastMessage = Color(0xff898989);

  static Color messengerScreenPeopleTabButtonOn = messengerScreenActionButtonColor;
  static Color messengerScreenPeopleTabButtonOff = Color(0xff010101);
  static Color messengerScreenPeopleTabTextOn = Color(0xffFFFFFF);
  static Color messengerScreenPeopleTabTextOff = Color(0xff7F7F7F);
  static Color messengerScreenPeoplePrimaryText = Color(0xffFFFFFF);
  static Color messengerScreenPeopleSecondaryText = Color(0xff898989);
  static Color messengerScreenPeopleAddToStoryButton = Color(0xff19A2FE);
  static Color messengerScreenPeopleAddToStoryText = Color(0xffFFFFFF);
  static Color messengerScreenPeopleActiveWaveButton = messengerScreenActionButtonColor;
  static Color messengerScreenPeopleActiveWaveIcon = Color(0xffFFFFFF);


  //chatScreen
  static Color chatScreenBackgroundColor = messengerScreenBackgroundColor;
  static Color chatScreenButtonColor = Color(0xff16A5FB);
  static Color chatScreenSentMessage = chatScreenButtonColor;
  static Color chatScreenReceivedMessage = messengerScreenActionButtonColor;
  static Color chatScreenPrimaryText = messengerScreenPeoplePrimaryText;
  static Color chatScreenSecondaryText = messengerScreenPeopleSecondaryText;
}
