import 'package:fake_messenger/CustomColor.dart';
import 'package:fake_messenger/MessageList.dart';
import 'file:///E:/Coding/Flutter/fake_chrome/lib/ScreenSize.dart';
import 'package:flutter/material.dart';

class ChatScreen extends StatefulWidget {
  final ContactForm contact;

  ChatScreen(this.contact);

  @override
  State<StatefulWidget> createState() {
    return ChatScreenState();
  }
}

class ChatScreenState extends State<ChatScreen> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        await Navigator.of(context).popAndPushNamed("/messengerScreen");
        return true;
      },
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Container(
          child: Column(
            children: <Widget>[
              //Space for status bar
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              //build actual body
              buildBody(),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildBody() {
    return Container(
      height: ScreenSize.safeBlockVerticalSize * 100,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      color: CustomColor.chatScreenBackgroundColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: ScreenSize.safeBlockVerticalSize * 8,
            width: ScreenSize.safeBlockHorizontalSize * 100,
            padding: EdgeInsets.symmetric(
                vertical: ScreenSize.safeBlockVerticalSize * 0.5,
                horizontal: ScreenSize.safeBlockHorizontalSize * 3),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: ScreenSize.safeBlockHorizontalSize * 6,
                  width: ScreenSize.safeBlockHorizontalSize * 6,
                  child: IconButton(
                    padding: EdgeInsets.zero,
                    icon: Icon(Icons.arrow_back),
                    iconSize: ScreenSize.safeBlockHorizontalSize * 6,
                    color: CustomColor.chatScreenButtonColor,
                    onPressed: () {
                      Navigator.of(context).popAndPushNamed("/messengerScreen");
                    },
                  ),
                ),
                SizedBox(
                  width: ScreenSize.safeBlockHorizontalSize * 2,
                ),
                Container(
                  height: ScreenSize.safeBlockHorizontalSize * 10,
                  width: ScreenSize.safeBlockHorizontalSize * 10,
                  child: Stack(
                    children: <Widget>[
                      Container(
                        height: ScreenSize.safeBlockVerticalSize * 5,
                        width: ScreenSize.safeBlockVerticalSize * 5,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: CustomColor.messengerScreenNoAvaButton,
                        ),
                        child: Container(
                          height: ScreenSize.safeBlockVerticalSize * 5,
                          child: (widget.contact.avatar.isNotEmpty)
                              ? CircleAvatar(
                                  backgroundImage:
                                      ExactAssetImage(widget.contact.avatar),
                                )
                              : Icon(
                                  Icons.person,
                                  size: ScreenSize.safeBlockVerticalSize * 3,
                                  color: Colors.white,
                                ),
                        ),
                      ),
                      (widget.contact.isOnline)
                          ? Align(
                              alignment: Alignment(1.0, 1.0),
                              child: Container(
                                height: ScreenSize.safeBlockVerticalSize * 2.3,
                                width: ScreenSize.safeBlockVerticalSize * 2.3,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: CustomColor
                                      .messengerScreenBackgroundColor,
                                ),
                                child: Center(
                                  child: Container(
                                    height:
                                        ScreenSize.safeBlockVerticalSize * 1.5,
                                    width:
                                        ScreenSize.safeBlockVerticalSize * 1.5,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: CustomColor
                                          .messengerScreenOnlineIndicator,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ),
                SizedBox(
                  width: ScreenSize.safeBlockHorizontalSize * 2,
                ),
                Container(
                  width: ScreenSize.safeBlockHorizontalSize * 40,
                  child: Text(
                    (!widget.contact.isGroup &&
                            widget.contact.nickname.isNotEmpty)
                        ? widget.contact.nickname
                        : widget.contact.fullName,
                    style: TextStyle(
                      fontSize: ScreenSize.safeBlockVerticalSize * 2.2,
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                SizedBox(
                  width: ScreenSize.safeBlockHorizontalSize * 5,
                ),
                Container(
                  height: ScreenSize.safeBlockVerticalSize * 7,
                  width: ScreenSize.horizontalBlockSize * 29,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: ScreenSize.safeBlockHorizontalSize * 7,
                        width: ScreenSize.safeBlockHorizontalSize * 7,
                        child: IconButton(
                          padding: EdgeInsets.zero,
                          icon: Icon(Icons.call),
                          iconSize: ScreenSize.safeBlockHorizontalSize * 7,
                          color: CustomColor.chatScreenButtonColor,
                          onPressed: () {},
                        ),
                      ),
                      Container(
                        height: ScreenSize.safeBlockHorizontalSize * 7,
                        width: ScreenSize.safeBlockHorizontalSize * 7,
                        child: IconButton(
                          padding: EdgeInsets.zero,
                          icon: Icon(Icons.videocam),
                          iconSize: ScreenSize.safeBlockHorizontalSize * 7,
                          color: CustomColor.chatScreenButtonColor,
                          onPressed: () {},
                        ),
                      ),
                      Container(
                        height: ScreenSize.safeBlockHorizontalSize * 7,
                        width: ScreenSize.safeBlockHorizontalSize * 7,
                        child: IconButton(
                          padding: EdgeInsets.zero,
                          icon: Icon(Icons.info),
                          iconSize: ScreenSize.safeBlockHorizontalSize * 7,
                          color: CustomColor.chatScreenButtonColor,
                          onPressed: () {},
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 86,
            width: ScreenSize.safeBlockHorizontalSize * 100,
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 6,
            width: ScreenSize.safeBlockHorizontalSize * 100,
            padding: EdgeInsets.symmetric(
                vertical: ScreenSize.safeBlockVerticalSize * 0.5,
                horizontal: ScreenSize.safeBlockHorizontalSize * 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: ScreenSize.safeBlockHorizontalSize * 6.5,
                  width: ScreenSize.safeBlockHorizontalSize * 6.5,
                  child: IconButton(
                    padding: EdgeInsets.zero,
                    icon: Icon(Icons.menu),
                    iconSize: ScreenSize.safeBlockHorizontalSize * 6.5,
                    color: CustomColor.chatScreenButtonColor,
                    onPressed: () {},
                  ),
                ),
                Container(
                  height: ScreenSize.safeBlockHorizontalSize * 6.5,
                  width: ScreenSize.safeBlockHorizontalSize * 6.5,
                  child: IconButton(
                    padding: EdgeInsets.zero,
                    icon: Icon(Icons.camera_alt),
                    iconSize: ScreenSize.safeBlockHorizontalSize * 6.5,
                    color: CustomColor.chatScreenButtonColor,
                    onPressed: () {},
                  ),
                ),
                Container(
                  height: ScreenSize.safeBlockHorizontalSize * 6.5,
                  width: ScreenSize.safeBlockHorizontalSize * 6.5,
                  child: IconButton(
                    padding: EdgeInsets.zero,
                    icon: Icon(Icons.photo),
                    iconSize: ScreenSize.safeBlockHorizontalSize * 6.5,
                    color: CustomColor.chatScreenButtonColor,
                    onPressed: () {},
                  ),
                ),
                Container(
                  height: ScreenSize.safeBlockHorizontalSize * 6.5,
                  width: ScreenSize.safeBlockHorizontalSize * 6.5,
                  child: IconButton(
                    padding: EdgeInsets.zero,
                    icon: Icon(Icons.mic),
                    iconSize: ScreenSize.safeBlockHorizontalSize * 6.5,
                    color: CustomColor.chatScreenButtonColor,
                    onPressed: () {},
                  ),
                ),
                Container(
                  height: ScreenSize.safeBlockHorizontalSize * 7.5,
                  width: ScreenSize.safeBlockHorizontalSize * 45,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: CustomColor.chatScreenReceivedMessage,
                  ),
                  child: MaterialButton(
                    padding: EdgeInsets.symmetric(
                        vertical: ScreenSize.safeBlockHorizontalSize * 0.5,
                        horizontal: ScreenSize.safeBlockHorizontalSize * 1),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                            height: ScreenSize.safeBlockHorizontalSize * 6.5,
                            width: ScreenSize.safeBlockHorizontalSize * 7.5,
                            child: Center(
                              child: Text(
                                "Aa",
                                style: TextStyle(
                                  color: CustomColor.chatScreenSecondaryText,
                                  fontSize:
                                      ScreenSize.safeBlockHorizontalSize * 4,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            )),
                        Container(
                          height: ScreenSize.safeBlockHorizontalSize * 6.5,
                          width: ScreenSize.safeBlockHorizontalSize * 6.5,
                          child: IconButton(
                            padding: EdgeInsets.zero,
                            icon: Icon(Icons.insert_emoticon),
                            iconSize: ScreenSize.safeBlockHorizontalSize * 6.5,
                            color: CustomColor.chatScreenButtonColor,
                            onPressed: () {},
                          ),
                        ),
                      ],
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(25.0),
                    ),
                    onPressed: () {
                      showAnswer();
                    },
                  ),
                ),
                Container(
                  height: ScreenSize.safeBlockHorizontalSize * 6.5,
                  width: ScreenSize.safeBlockHorizontalSize * 6.5,
                  child: IconButton(
                    padding: EdgeInsets.zero,
                    icon: Icon(Icons.pan_tool),
                    iconSize: ScreenSize.safeBlockHorizontalSize * 6.5,
                    color: CustomColor.chatScreenButtonColor,
                    onPressed: () {},
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<String> showAnswer() async {
    return await showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return Container(
          width: ScreenSize.safeBlockHorizontalSize * 100,
          padding: EdgeInsets.symmetric(
              vertical: ScreenSize.verticalBlockSize * 3,
              horizontal: ScreenSize.safeBlockHorizontalSize * 5),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(25.0),
                topLeft: Radius.circular(25.0)),
            color: CustomColor.chatScreenBackgroundColor,
          ),
          child: Wrap(
            children: <Widget>[
              Container(
                height: ScreenSize.verticalBlockSize * 10,
                width: ScreenSize.safeBlockHorizontalSize * 90,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(25.0),
                    topLeft: Radius.circular(25.0),
                    bottomLeft: Radius.circular(25.0),
                  ),
                  color: CustomColor.chatScreenSentMessage,
                ),
              ),
              Container(
                height: ScreenSize.verticalBlockSize * 1,
                width: ScreenSize.safeBlockHorizontalSize * 90,
                color: Colors.transparent,
              ),
              Container(
                height: ScreenSize.verticalBlockSize * 10,
                width: ScreenSize.safeBlockHorizontalSize * 90,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(25.0),
                    topLeft: Radius.circular(25.0),
                    bottomLeft: Radius.circular(25.0),
                  ),
                  color: CustomColor.chatScreenSentMessage,
                ),
              ),
              Container(
                height: ScreenSize.verticalBlockSize * 3,
                width: ScreenSize.safeBlockHorizontalSize * 90,
                color: Colors.transparent,
              ),
              Container(
                height: ScreenSize.verticalBlockSize * 6,
                width: ScreenSize.safeBlockHorizontalSize * 90,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25.0),
                  color: CustomColor.chatScreenReceivedMessage,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
