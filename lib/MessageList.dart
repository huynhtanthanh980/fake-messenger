class ContactForm {
  String avatar;
  String name;
  String fullName;
  String nickname;
  List<MessageForm> messageList;
  String story;
  bool hadSeen;
  bool isOnline;
  bool isGroup;

  ContactForm(this.avatar, this.name, this.fullName, this.nickname,
      this.messageList, this.story,
      this.hadSeen,
      this.isOnline, this.isGroup,);
}

class MessageForm {
  String message;
  bool isSent;
  DateTime time;

  MessageForm(this.message, this.isSent, this.time,);
}

List<ContactForm> contactList = [
  ContactForm(
    "asset/Nhật Quỳnh.png",
    "Quỳnh",
    "Phàn Gia Nhật Quỳnh",
    "Con vịt mặt khó ưa",
    [MessageForm("hidsfaaaaaaaaaaaaaaaahi", false, DateTime.parse("2018-01-10 20:18:04")), MessageForm("This is a test", true, DateTime.parse("2019-01-10 20:10:59")),],
    "",
    true,
    true,
    false,),
  ContactForm(
    "asset/HeoKun TheBest.jpg",
    "HeoKun",
    "HeoKun TheBest",
    "",
    [],
    "",
    false,
    false,
    false,),
  ContactForm(
    "asset/Duy Lê.jpg",
    "Duy",
    "Duy Lê",
    "",
    [],
    "",
    true,
    false,
    false,),
  ContactForm(
    "asset/Động TN.JPG",
    "",
    "Động Tờ Nờ chi nhánh 2",
    "",
    [],
    "",
    false,
    true,
    true,),
  ContactForm(
    "asset/Nguyễn Đình Thắng.jpg",
    "Thắng",
    "Nguyễn Đình Thắng",
    "",
    [],
    "",
    true,
    true,
    false,),
  ContactForm(
    "asset/Sang Nguyễn.jpg",
    "Sang",
    "Sang Nguyễn",
    "",
    [],
    "",
    true,
    true,
    false,),
  ContactForm(
    "asset/Nguyen Khuong.jpg",
    "Khuong",
    "Nguyen Khuong",
    "",
    [],
    "",
    true,
    true,
    false,),
  ContactForm(
    "asset/Phạm Nhân.jpg",
    "Nhân",
    "Phạm Nhân",
    "",
    [],
    "",
    true,
    true,
    false,),
  ContactForm(
    "asset/Văn Stephen.jpg",
    "Văn",
    "Văn Stephen",
    "",
    [],
    "",
    true,
    true,
    false,),
  ContactForm(
    "asset/Hạ Mỹ Lai Nguyễn.jpg",
    "Hạ Mỹ",
    "Hạ Mỹ Lai Nguyễn",
    "",
    [],
    "",
    true,
    true,
    false,),
  ContactForm(
    "asset/Đức Trí.jpg",
    "Trí",
    "Đức Trí",
    "",
    [],
    "",
    true,
    true,
    false,),
  ContactForm(
    "asset/Trần Hoàng Thịnh.jpg",
    "Thịnh",
    "Trần Hoàng Thịnh",
    "",
    [],
    "",
    true,
    true,
    false,),
];
