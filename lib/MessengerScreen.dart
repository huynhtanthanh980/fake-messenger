import 'dart:core';

import 'package:fake_messenger/ChatScreen.dart';
import 'package:fake_messenger/ContactController.dart';
import 'package:fake_messenger/CustomColor.dart';
import 'package:fake_messenger/MessageList.dart';
import 'file:///E:/Coding/Flutter/fake_chrome/lib/ScreenSize.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class MessengerScreen extends StatefulWidget {
  @override
  _MessengerScreenState createState() => _MessengerScreenState();
}

class _MessengerScreenState extends State<MessengerScreen> {
  bool isInChatScreen = true;
  bool isInStoriesTab = true;

  ContactController contactController = new ContactController(contactList);

  @override
  void initState() {
    super.initState();
    contactController.init();
  }

  // ignore: missing_return
  Future<int> whenNotZero(Stream<double> source) async {
    await for (double value in source) {
      if (value > 0) {
        return 1; //if screen size is found, return 1
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (ScreenSize.isInit) {
      return buildBody();
    } else {
      return FutureBuilder(
        future: whenNotZero(Stream<double>.periodic(Duration(milliseconds: 50),
                (x) => MediaQuery.of(context).size.width)),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            ScreenSize().init(context);
            return buildBody();
          }
          return Container(
            child: CircularProgressIndicator(),
          );
        },
      );
    }
  }

  Widget buildBody() {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: CustomColor.messengerScreenBackgroundColor,
      body: Container(
        child: Column(
          children: <Widget>[
            //Space for status bar
            SizedBox(
              height: MediaQuery.of(context).padding.top,
            ),
            //Appbar
            Container(
              height: ScreenSize.safeBlockVerticalSize * 9,
              width: ScreenSize.safeBlockHorizontalSize * 100,
              padding: EdgeInsets.symmetric(
                  horizontal: ScreenSize.safeBlockHorizontalSize * 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  //Ava and chat or people text
                  Container(
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: ScreenSize.safeBlockVerticalSize * 5,
                          width: ScreenSize.safeBlockVerticalSize * 5,
                          child: FlatButton(
                            onPressed: () {},
                            child: CircleAvatar(
                              backgroundImage:
                              ExactAssetImage("asset/myAva.jpg"),
                            ),
                            shape: new CircleBorder(),
                            padding: EdgeInsets.zero,
                          ),
                        ),
                        SizedBox(
                          width: ScreenSize.safeBlockHorizontalSize * 3,
                        ),
                        Text(
                          (isInChatScreen) ? "Chats" : "People",
                          style: TextStyle(
                            fontSize: ScreenSize.safeBlockVerticalSize * 3.5,
                            color: Colors.white,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      ],
                    ),
                  ),

                  //action button
                  Container(
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: ScreenSize.safeBlockVerticalSize * 5,
                          width: ScreenSize.safeBlockVerticalSize * 5,
                          child: RawMaterialButton(
                            onPressed: () {},
                            child: new Icon(
                              (isInChatScreen)
                                  ? Icons.camera_alt
                                  : Icons.contacts,
                              color: Colors.white,
                              size: ScreenSize.safeBlockVerticalSize * 3,
                            ),
                            elevation: 0.0,
                            shape: new CircleBorder(),
                            fillColor: CustomColor.messengerScreenActionButtonColor,
                            padding: EdgeInsets.zero,
                          ),
                        ),
                        SizedBox(
                          width: ScreenSize.safeBlockHorizontalSize * 3,
                        ),
                        Container(
                          height: ScreenSize.safeBlockVerticalSize * 5,
                          width: ScreenSize.safeBlockVerticalSize * 5,
                          child: RawMaterialButton(
                            onPressed: () {},
                            child: new Icon(
                              (isInChatScreen)
                                  ? Icons.create
                                  : Icons.person_add,
                              color: Colors.white,
                              size: ScreenSize.safeBlockVerticalSize * 3,
                            ),
                            elevation: 0.0,
                            shape: new CircleBorder(),
                            fillColor: CustomColor.messengerScreenActionButtonColor,
                            padding: EdgeInsets.zero,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),

            //build chat list
            (isInChatScreen) ? buildChatPage() : buildPeoplePage(),

            //bottom navigate button
            Container(
              height: ScreenSize.safeBlockVerticalSize * 7,
              width: ScreenSize.safeBlockHorizontalSize * 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: ScreenSize.safeBlockVerticalSize * 7,
                    width: ScreenSize.safeBlockHorizontalSize * 50,
                    child: MaterialButton(
                      padding: EdgeInsets.symmetric(
                        horizontal: ScreenSize.safeBlockHorizontalSize * 17.5,
                      ),
                      child: Stack(
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(
                                height: ScreenSize.safeBlockVerticalSize * 1,
                              ),
                              Container(
                                height: ScreenSize.safeBlockVerticalSize * 3,
                                width: ScreenSize.safeBlockHorizontalSize * 15,
                                child: Icon(
                                  Icons.mode_comment,
                                  color: (isInChatScreen)
                                      ? CustomColor.messengerScreenNavigatorButtonOn
                                      : CustomColor.messengerScreenNavigatorButtonOff,
                                  size: ScreenSize.safeBlockVerticalSize * 3,
                                ),
                              ),
                              Container(
                                height: ScreenSize.safeBlockVerticalSize * 2,
                                width: ScreenSize.safeBlockHorizontalSize * 15,
                                child: Center(
                                  child: Text(
                                    "Chats",
                                    style: TextStyle(
                                      fontSize:
                                      ScreenSize.safeBlockVerticalSize *
                                          1.8,
                                      color: (isInChatScreen)
                                          ? CustomColor.messengerScreenNavigatorButtonOn
                                          : CustomColor.messengerScreenNavigatorButtonOff,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: ScreenSize.safeBlockVerticalSize * 1,
                              ),
                            ],
                          ),
                          Align(
                            alignment: Alignment(0.6, -1.0),
                            child: Container(
                              height: ScreenSize.safeBlockVerticalSize * 2.7,
                              width: ScreenSize.safeBlockVerticalSize * 2.7,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: CustomColor.messengerScreenBackgroundColor,
                              ),
                              child: Center(
                                child: Container(
                                  height:
                                  ScreenSize.safeBlockVerticalSize * 2.0,
                                  width: ScreenSize.safeBlockVerticalSize * 2.0,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: CustomColor.messengerScreenOnlineBubbleBackground,
                                  ),
                                  child: Center(
                                    child: Text(
                                      "6",
                                      style: TextStyle(
                                        fontSize:
                                        ScreenSize.safeBlockVerticalSize *
                                            1.8,
                                        fontWeight: FontWeight.w700,
                                        color: CustomColor.messengerScreenOnlineNumberText,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      onPressed: () {
                        setState(() {
                          isInChatScreen = true;
                        });
                      },
                    ),
                  ),
                  Container(
                    height: ScreenSize.safeBlockVerticalSize * 7,
                    width: ScreenSize.safeBlockHorizontalSize * 50,
                    child: MaterialButton(
                      padding: EdgeInsets.symmetric(
                        horizontal: ScreenSize.safeBlockHorizontalSize * 17.5,
                      ),
                      child: Stack(
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                height: ScreenSize.safeBlockVerticalSize * 3,
                                width: ScreenSize.safeBlockHorizontalSize * 15,
                                child: Icon(
                                  Icons.people,
                                  color: (isInChatScreen)
                                      ? CustomColor.messengerScreenNavigatorButtonOff
                                      : Colors.white,
                                  size: ScreenSize.safeBlockVerticalSize * 4,
                                ),
                              ),
                              Container(
                                height: ScreenSize.safeBlockVerticalSize * 2,
                                width: ScreenSize.safeBlockHorizontalSize * 15,
                                child: Center(
                                  child: Text(
                                    "People",
                                    style: TextStyle(
                                      fontSize:
                                      ScreenSize.safeBlockVerticalSize *
                                          1.8,
                                      color: (isInChatScreen)
                                          ? CustomColor.messengerScreenNavigatorButtonOff
                                          : Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Align(
                            alignment: Alignment(1.0, -1.0),
                            child: Container(
                              height: ScreenSize.safeBlockVerticalSize * 2.7,
                              width: ScreenSize.safeBlockVerticalSize * 3.6,
                              decoration: BoxDecoration(
                                borderRadius: new BorderRadius.circular(12.0),
                                color: CustomColor.messengerScreenBackgroundColor,
                              ),
                              child: Center(
                                child: Container(
                                  height:
                                  ScreenSize.safeBlockVerticalSize * 2.0,
                                  width: ScreenSize.safeBlockVerticalSize * 2.9,
                                  decoration: BoxDecoration(
                                    borderRadius:
                                    new BorderRadius.circular(12.0),
                                    color: CustomColor.messengerScreenPeopleBubbleBackground,
                                  ),
                                  child: Center(
                                    child: Text(
                                      "13",
                                      style: TextStyle(
                                        fontSize:
                                        ScreenSize.safeBlockVerticalSize *
                                            1.8,
                                        fontWeight: FontWeight.w700,
                                        color: CustomColor.messengerScreenPeopleTextNumber,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      onPressed: () {
                        setState(() {
                          isInChatScreen = false;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildChatPage() {
    return Container(
      height: ScreenSize.safeBlockVerticalSize * 84,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      child: NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (overscroll) {
          overscroll.disallowGlow();
          return;
        },
        child: ConstrainedBox(
          constraints: BoxConstraints(
            maxHeight: ScreenSize.safeBlockVerticalSize * 84,
          ),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  height: ScreenSize.safeBlockVerticalSize * 5,
                  width: ScreenSize.safeBlockHorizontalSize * 100,
                  padding: EdgeInsets.symmetric(
                      horizontal: ScreenSize.safeBlockHorizontalSize * 5),
                  child: Container(
                    height: ScreenSize.safeBlockVerticalSize * 4,
                    width: ScreenSize.safeBlockVerticalSize * 5,
                    child: RawMaterialButton(
                      onPressed: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: ScreenSize.safeBlockHorizontalSize * 3,
                          ),
                          Icon(
                            Icons.search,
                            color: CustomColor.messengerScreenSearchText,
                            size: ScreenSize.safeBlockVerticalSize * 3.5,
                          ),
                          SizedBox(
                            width: ScreenSize.safeBlockHorizontalSize * 2,
                          ),
                          Text(
                            "Search",
                            style: TextStyle(
                              color: CustomColor.messengerScreenSearchText,
                              fontSize: ScreenSize.safeBlockVerticalSize * 2.6,
                            ),
                          )
                        ],
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0),
                      ),
                      fillColor: CustomColor.messengerScreenSearchBox,
                      padding: EdgeInsets.zero,
                    ),
                  ),
                ),
                Container(
                  height: ScreenSize.safeBlockVerticalSize * 15,
                  width: ScreenSize.safeBlockHorizontalSize * 100,
                  padding: EdgeInsets.symmetric(
                    vertical: ScreenSize.safeBlockVerticalSize * 2,
                  ),
                  child: NotificationListener<OverscrollIndicatorNotification>(
                    onNotification: (overscroll) {
                      overscroll.disallowGlow();
                      return;
                    },
                    child: ListView.builder(
                      itemCount: contactList.length + 1,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (BuildContext context, int index) {
                        return (index != 0 && contactList[index - 1].isGroup)
                            ? Container()
                            : Container(
                          height: ScreenSize.safeBlockVerticalSize * 10,
                          width: (index == 0)
                              ? ScreenSize.safeBlockHorizontalSize * 22
                              : ScreenSize.safeBlockHorizontalSize * 18,
                          child: Column(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                height:
                                ScreenSize.safeBlockVerticalSize * 7,
                                width:
                                ScreenSize.safeBlockVerticalSize * 7,
                                child: Stack(
                                  children: <Widget>[
                                    Container(
                                      height: ScreenSize
                                          .safeBlockVerticalSize *
                                          7,
                                      width: ScreenSize
                                          .safeBlockVerticalSize *
                                          7,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: CustomColor.messengerScreenNoAvaButton,
                                      ),
                                      child: Container(
                                        height: ScreenSize
                                            .safeBlockVerticalSize *
                                            8,
                                        child: (index != 0 &&
                                            contactList[index - 1]
                                                .avatar
                                                .isNotEmpty)
                                            ? CircleAvatar(
                                          backgroundImage:
                                          ExactAssetImage(
                                              contactList[
                                              index - 1]
                                                  .avatar),
                                        )
                                            : Icon(
                                          (index == 0)
                                              ? Icons.add
                                              : Icons.person,
                                          size: ScreenSize
                                              .safeBlockVerticalSize *
                                              5,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                    (index != 0 &&
                                        contactList[index - 1]
                                            .isOnline)
                                        ? Align(
                                      alignment:
                                      Alignment(1.0, 1.0),
                                      child: Container(
                                        height: ScreenSize
                                            .safeBlockVerticalSize *
                                            2.5,
                                        width: ScreenSize
                                            .safeBlockVerticalSize *
                                            2.5,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: CustomColor
                                              .messengerScreenBackgroundColor,
                                        ),
                                        child: Center(
                                          child: Container(
                                            height: ScreenSize
                                                .safeBlockVerticalSize *
                                                1.5,
                                            width: ScreenSize
                                                .safeBlockVerticalSize *
                                                1.5,
                                            decoration:
                                            BoxDecoration(
                                              shape:
                                              BoxShape.circle,
                                              color: CustomColor
                                                  .messengerScreenOnlineIndicator,
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                        : Container(),
                                  ],
                                ),
                              ),
                              Container(
                                height:
                                ScreenSize.safeBlockVerticalSize * 3,
                                width:
                                ScreenSize.safeBlockHorizontalSize *
                                    18,
                                child: Center(
                                  child: Text(
                                    (index == 0)
                                        ? "Your Story"
                                        : contactList[index - 1].name,
                                    style: TextStyle(
                                      fontSize: ScreenSize
                                          .safeBlockVerticalSize *
                                          2,
                                      color: CustomColor.messengerScreenSearchText,
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ),
                // ignore: sdk_version_ui_as_code
                ...buildMessageTile(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> buildMessageTile() {
    List<Widget> list = [];

    for (ContactForm item in contactList) {
      Widget messageTile = Container(
        height: ScreenSize.safeBlockVerticalSize * 8,
        width: ScreenSize.safeBlockHorizontalSize * 100,
        child: MaterialButton(
          padding: EdgeInsets.symmetric(
              vertical: ScreenSize.safeBlockVerticalSize * 0.5,
              horizontal: ScreenSize.safeBlockHorizontalSize * 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: ScreenSize.safeBlockHorizontalSize * 85,
                child: Row(
                  children: <Widget>[
                    Container(
                      height: ScreenSize.safeBlockHorizontalSize * 13,
                      width: ScreenSize.safeBlockHorizontalSize * 13,
                      child: Stack(
                        children: <Widget>[
                          Container(
                            height: ScreenSize.safeBlockVerticalSize * 6.5,
                            width: ScreenSize.safeBlockVerticalSize * 6.5,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: CustomColor.messengerScreenNoAvaButton,
                            ),
                            child: Container(
                              height: ScreenSize.safeBlockVerticalSize * 6,
                              child: (item.avatar.isNotEmpty)
                                  ? CircleAvatar(
                                backgroundImage:
                                ExactAssetImage(item.avatar),
                              )
                                  : Icon(
                                Icons.person,
                                size:
                                ScreenSize.safeBlockVerticalSize * 4,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          (item.isOnline)
                              ? Align(
                            alignment: Alignment(1.0, 1.0),
                            child: Container(
                              height:
                              ScreenSize.safeBlockVerticalSize * 2.5,
                              width:
                              ScreenSize.safeBlockVerticalSize * 2.5,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: CustomColor.messengerScreenBackgroundColor,
                              ),
                              child: Center(
                                child: Container(
                                  height:
                                  ScreenSize.safeBlockVerticalSize *
                                      1.5,
                                  width:
                                  ScreenSize.safeBlockVerticalSize *
                                      1.5,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: CustomColor.messengerScreenOnlineIndicator,
                                  ),
                                ),
                              ),
                            ),
                          )
                              : Container(),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: ScreenSize.safeBlockHorizontalSize * 3,
                    ),
                    Container(
                      width: ScreenSize.safeBlockHorizontalSize * 68,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: ScreenSize.safeBlockHorizontalSize * 68,
                            child: Text(
                              (!item.isGroup && item.nickname.isNotEmpty)
                                  ? item.nickname
                                  : item.fullName,
                              style: TextStyle(
                                fontSize:
                                ScreenSize.safeBlockVerticalSize * 2.3,
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          Container(
                            width: ScreenSize.safeBlockHorizontalSize * 68,
                            child: (item.messageList.length > 0) ? Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                ConstrainedBox(
                                  constraints: BoxConstraints(
                                    maxWidth:
                                    ScreenSize.safeBlockHorizontalSize * 42,
                                  ),
                                  child: Container(
                                    child: Text(
                                      item.messageList.first.message,
                                      style: TextStyle(
                                        fontSize:
                                        ScreenSize.safeBlockVerticalSize *
                                            2.0,
                                        color: CustomColor.messengerScreenLastMessage,
                                      ),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                ),
                                Container(
                                  width: ScreenSize.safeBlockHorizontalSize * 3,
                                  child: Center(
                                    child: Text(
                                      "•",
                                      style: TextStyle(
                                        fontSize:
                                        ScreenSize.safeBlockVerticalSize *
                                            1.5,
                                        color: CustomColor.messengerScreenLastMessage,
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  width:
                                  ScreenSize.safeBlockHorizontalSize * 23,
                                  child: Text(
                                    getLastMessageDate(
                                        item.messageList.first.time),
                                    style: TextStyle(
                                      fontSize:
                                      ScreenSize.safeBlockVerticalSize *
                                          2.0,
                                      color: CustomColor.messengerScreenLastMessage,
                                    ),
                                  ),
                                ),
                              ],
                            ) : ConstrainedBox(
                              constraints: BoxConstraints(
                                maxWidth:
                                ScreenSize.safeBlockHorizontalSize * 60,
                              ),
                              child: Container(
                                child: Text(
                                  "Say hi to your new facebook friend...",
                                  style: TextStyle(
                                    fontSize:
                                    ScreenSize.safeBlockVerticalSize *
                                        2.0,
                                    color: CustomColor.messengerScreenLastMessage,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: ScreenSize.safeBlockHorizontalSize * 5,
                width: ScreenSize.safeBlockHorizontalSize * 5,
                child: (item.hadSeen)
                    ? Container(
                  height: ScreenSize.safeBlockHorizontalSize * 5,
                  width: ScreenSize.safeBlockHorizontalSize * 5,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: CustomColor.messengerScreenNoAvaButton,
                  ),
                  child: Container(
                    height: ScreenSize.safeBlockHorizontalSize * 5,
                    width: ScreenSize.safeBlockHorizontalSize * 5,
                    child: (item.avatar.isNotEmpty)
                        ? CircleAvatar(
                      backgroundImage: ExactAssetImage(item.avatar),
                    )
                        : Icon(
                      Icons.person,
                      size: ScreenSize.safeBlockVerticalSize * 1.8,
                      color: Colors.white,
                    ),
                  ),
                )
                    : Container(),
              ),
            ],
          ),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => ChatScreen(item)));
          },
        ),
      );
      list.add(messageTile);
    }

    return list;
  }

  String getLastMessageDate(DateTime sentDate) {
    String result = "";

    if (DateTime
        .now()
        .difference(sentDate)
        .inDays
        .abs() <=
        365) {
      if (DateTime
          .now()
          .difference(sentDate)
          .inDays
          .abs() <=
          1) {
        print(DateTime
            .now()
            .difference(sentDate)
            .inDays
            .abs());
        result = DateFormat("hh:mm").format(sentDate);
      }
      else {
        result = getMonthText(sentDate.month) + " " + sentDate.day.toString();
      }
    }
    else {
      result = getMonthText(sentDate.month) + " " +
          DateFormat("dd").format(sentDate) + ", " + sentDate.year.toString();
    }

    return result;
  }

  String getMonthText(int month) {
    switch (month) {
      case 1:
        return "Jan";
      case 2:
        return "Feb";
      case 3:
        return "Mar";
      case 4:
        return "Apr";
      case 5:
        return "May";
      case 6:
        return "Jun";
      case 7:
        return "Jul";
      case 8:
        return "Aug";
      case 9:
        return "Sep";
      case 10:
        return "Oct";
      case 11:
        return "Nov";
      case 12:
        return "Sep";
    }

    return "";
  }

  Widget buildPeoplePage() {
    return Container(
      height: ScreenSize.safeBlockVerticalSize * 84,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: ScreenSize.safeBlockVerticalSize * 5,
            width: ScreenSize.safeBlockHorizontalSize * 100,
            padding: EdgeInsets.symmetric(
                horizontal: ScreenSize.safeBlockHorizontalSize * 5),
            child: Row(
              children: <Widget>[
                Container(
                  height: ScreenSize.safeBlockVerticalSize * 3.5,
                  width: ScreenSize.safeBlockHorizontalSize * 45,
                  child: RawMaterialButton(
                    padding: EdgeInsets.zero,
                    elevation: 0.0,
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    child: Center(
                      child: Text(
                        "STORIES",
                        style: TextStyle(
                            color: (isInStoriesTab)
                                ? CustomColor.messengerScreenPeopleTabTextOn
                                : CustomColor.messengerScreenPeopleTabTextOff,
                            fontSize: ScreenSize.safeBlockVerticalSize * 1.8,
                            fontWeight: FontWeight.w800),
                      ),
                    ),
                    onPressed: () {
                      setState(() {
                        isInStoriesTab = true;
                      });
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(20.0),
                    ),
                    fillColor: (isInStoriesTab)
                        ? CustomColor.messengerScreenPeopleTabButtonOn
                        : CustomColor.messengerScreenPeopleTabButtonOff,
                  ),
                ),
                Container(
                  height: ScreenSize.safeBlockVerticalSize * 3.5,
                  width: ScreenSize.safeBlockHorizontalSize * 45,
                  child: RawMaterialButton(
                    padding: EdgeInsets.zero,
                    elevation: 0.0,
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    child: Center(
                      child: Text(
                        "ACTIVE(" +
                            contactController.onlineCount.toString() +
                            ")",
                        style: TextStyle(
                            color: (!isInStoriesTab)
                                ? CustomColor.messengerScreenPeopleTabTextOn
                                : CustomColor.messengerScreenPeopleTabTextOff,
                            fontSize: ScreenSize.safeBlockVerticalSize * 1.8,
                            fontWeight: FontWeight.w800),
                      ),
                    ),
                    onPressed: () {
                      setState(() {
                        isInStoriesTab = false;
                      });
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(20.0),
                    ),
                    fillColor: (!isInStoriesTab)
                        ? CustomColor.messengerScreenPeopleTabButtonOn
                        : CustomColor.messengerScreenPeopleTabButtonOff,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: ScreenSize.safeBlockVerticalSize * 1,
          ),
          (isInStoriesTab) ? buildStoriesTab() : buildActiveTab(),
        ],
      ),
    );
  }

  Widget buildStoriesTab() {
    return Container(
      height: ScreenSize.safeBlockVerticalSize * 78,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: ScreenSize.safeBlockVerticalSize * 20,
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 3,
            width: ScreenSize.safeBlockHorizontalSize * 100,
            child: Center(
              child: Text(
                "Share a Story",
                style: TextStyle(
                  color: CustomColor.messengerScreenPeoplePrimaryText,
                  fontSize: ScreenSize.safeBlockVerticalSize * 2.5,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 10,
            width: ScreenSize.safeBlockHorizontalSize * 100,
            child: Center(
              child: Text(
                "Share photos, videos, and more for\n24 hours. When you and your friends\nshare stories, they'll show up here.",
                style: TextStyle(
                  color: CustomColor.messengerScreenPeopleSecondaryText,
                  fontSize: ScreenSize.safeBlockVerticalSize * 2,
                  fontWeight: FontWeight.w400,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          SizedBox(
            height: ScreenSize.safeBlockVerticalSize * 2,
          ),
          Container(
            height: ScreenSize.safeBlockVerticalSize * 4,
            width: ScreenSize.safeBlockHorizontalSize * 60,
            child: RawMaterialButton(
              padding: EdgeInsets.zero,
              elevation: 0.0,
              child: Center(
                child: Text(
                  "ADD TO STORY",
                  style: TextStyle(
                      color: CustomColor.messengerScreenPeopleAddToStoryText,
                      fontSize: ScreenSize.safeBlockVerticalSize * 1.8,
                      fontWeight: FontWeight.w600),
                ),
              ),
              onPressed: () {},
              shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0),
              ),
              fillColor: CustomColor.messengerScreenPeopleAddToStoryButton,
            ),
          ),
        ],
      ),
    );
  }

  Widget buildActiveTab() {
    return Container(
      height: ScreenSize.safeBlockVerticalSize * 78,
      width: ScreenSize.safeBlockHorizontalSize * 100,
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxHeight: ScreenSize.safeBlockVerticalSize * 84,
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: buildActiveTile(),
          ),
        ),
      ),
    );
  }

  List<Widget> buildActiveTile() {
    List<Widget> list = [];

    for (ContactForm item in contactList) {
      if (item.isOnline) {
        list.add(Container(
          height: ScreenSize.safeBlockVerticalSize * 8,
          width: ScreenSize.safeBlockHorizontalSize * 100,
          child: MaterialButton(
            padding: EdgeInsets.symmetric(
                vertical: ScreenSize.safeBlockVerticalSize * 0.5,
                horizontal: ScreenSize.safeBlockHorizontalSize * 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: ScreenSize.safeBlockVerticalSize * 5.5,
                  width: ScreenSize.safeBlockHorizontalSize * 80,
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: ScreenSize.safeBlockVerticalSize * 5.5,
                        width: ScreenSize.safeBlockVerticalSize * 5.5,
                        child: Stack(
                          children: <Widget>[
                            Container(
                              height: ScreenSize.safeBlockVerticalSize * 5,
                              width: ScreenSize.safeBlockVerticalSize * 5,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: CustomColor.messengerScreenNoAvaButton,
                              ),
                              child: Container(
                                height: ScreenSize.safeBlockVerticalSize * 4.5,
                                child: (item.avatar.isNotEmpty)
                                    ? CircleAvatar(
                                  backgroundImage:
                                  ExactAssetImage(item.avatar),
                                )
                                    : Icon(
                                  Icons.person,
                                  size: ScreenSize.safeBlockVerticalSize *
                                      4,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            (item.isOnline)
                                ? Align(
                              alignment: Alignment(1.0, 1.0),
                              child: Container(
                                height: ScreenSize.safeBlockVerticalSize *
                                    2.2,
                                width: ScreenSize.safeBlockVerticalSize *
                                    2.2,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: CustomColor.messengerScreenBackgroundColor,
                                ),
                                child: Center(
                                  child: Container(
                                    height:
                                    ScreenSize.safeBlockVerticalSize *
                                        1.3,
                                    width:
                                    ScreenSize.safeBlockVerticalSize *
                                        1.3,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: CustomColor.messengerScreenOnlineIndicator,
                                    ),
                                  ),
                                ),
                              ),
                            )
                                : Container(),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: ScreenSize.safeBlockVerticalSize * 2,
                      ),
                      Container(
                        width: ScreenSize.safeBlockHorizontalSize * 60,
                        child: Text(
                          item.fullName,
                          style: TextStyle(
                            fontSize: ScreenSize.safeBlockVerticalSize * 2.1,
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: ScreenSize.safeBlockVerticalSize * 4.5,
                  width: ScreenSize.safeBlockVerticalSize * 4.5,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: CustomColor.messengerScreenPeopleActiveWaveButton,
                  ),
                  child: MaterialButton(
                    padding: EdgeInsets.zero,
                    child: Icon(
                      Icons.pan_tool,
                      color: CustomColor.messengerScreenPeopleActiveWaveIcon,
                      size: ScreenSize.safeBlockVerticalSize * 2.2,
                    ),
                    shape: new CircleBorder(),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => ChatScreen(item)));
                    },
                  ),
                ),
              ],
            ),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => ChatScreen(item)));
            },
          ),
        ));
      }
    }

    return list;
  }
}
