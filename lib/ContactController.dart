import 'package:fake_messenger/MessageList.dart';

class ContactController {
  int onlineCount = 0;
  List<ContactForm> contactList;

  ContactController(this.contactList);

  void init() {
    for (ContactForm item in contactList) {
      if (item.isOnline) {
        onlineCount++;
      }
    }
  }
}
